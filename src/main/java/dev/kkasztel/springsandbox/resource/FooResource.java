package dev.kkasztel.springsandbox.resource;

import dev.kkasztel.springsandbox.persistence.entity.FooEntity;
import dev.kkasztel.springsandbox.persistence.repository.FooEmRepository;
import dev.kkasztel.springsandbox.persistence.repository.FooJdbcRepository;
import dev.kkasztel.springsandbox.persistence.repository.FooRepository;
import dev.kkasztel.springsandbox.persistence.repository.FooSdRepository;
import dev.kkasztel.springsandbox.persistence.repository.FooSpecRepository;
import io.vavr.collection.IndexedSeq;
import io.vavr.collection.Iterator;

import java.util.Locale;
import java.util.UUID;

import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static dev.kkasztel.springsandbox.resource.FooResource.Repo.HIBERNATE;
import static dev.kkasztel.springsandbox.resource.FooResource.Repo.JDBC;
import static dev.kkasztel.springsandbox.resource.FooResource.Repo.SD;
import static io.vavr.API.Map;
import static io.vavr.API.Seq;
import static io.vavr.API.Set;

@RestController
@RequestMapping("/foo")
@Slf4j
public class FooResource {

    private final Map<Repo, FooRepository> repoMap;
    private final FooSpecRepository specRepository;

    public FooResource(FooSdRepository fooSdRepository,
                       FooEmRepository fooEmRepository,
                       FooJdbcRepository fooJdbcRepository,
                       FooSpecRepository specRepository) {
        this.repoMap = Map(
                SD, fooSdRepository,
                HIBERNATE, fooEmRepository,
                JDBC, fooJdbcRepository
        );
        this.specRepository = specRepository;
    }

    @PostMapping("/{repo}")
    public void generate(@PathVariable Repo repo, @RequestParam int n) {
        repoMap.get(repo).forEach(r -> persist(n, r));
    }

    @GetMapping("/{repo}")
    public Seq<FooEntity> get(@PathVariable Repo repo) {
        return repoMap.get(repo)
                .map(this::get)
                .getOrElse(Seq());
    }

    @GetMapping("/find")
    public Seq<FooEntity> find() {
        return specRepository.findWithResources(Seq("bar", "foo"));
    }

    private void persist(int n, FooRepository fooRepository) {
        IndexedSeq<FooEntity> foos = generate(n);
        log.info("Saving random data");
        fooRepository.insert(foos);
        log.info("Random data saved");
    }

    private Seq<FooEntity> get(FooRepository fooRepository) {
        return fooRepository.findAll();
    }

    private IndexedSeq<FooEntity> generate(int n) {
        return Iterator.continually(UUID::randomUUID)
                .take(n)
                .map(i -> FooEntity.of(
                        i,
                        i.toString().toUpperCase(Locale.ENGLISH),
                        Set("foo", "bar", "bazz").toJavaSet()
                ))
                .toVector();
    }

    enum Repo {

        SD, HIBERNATE, JDBC
    }
}
