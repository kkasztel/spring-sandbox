package dev.kkasztel.springsandbox.persistence.repository;

import dev.kkasztel.springsandbox.persistence.entity.FooEntity;
import io.vavr.collection.IndexedSeq;

public interface FooRepository {

    void insert(IndexedSeq<FooEntity> fooEntities);

    IndexedSeq<FooEntity> findAll();
}
