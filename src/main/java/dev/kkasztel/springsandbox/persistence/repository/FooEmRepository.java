package dev.kkasztel.springsandbox.persistence.repository;

import dev.kkasztel.springsandbox.persistence.entity.FooEntity;
import io.vavr.collection.IndexedSeq;
import io.vavr.collection.Vector;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class FooEmRepository implements FooRepository {

    private final EntityManager entityManager;

    @Transactional
    public void insert(IndexedSeq<FooEntity> fooEntities) {
        fooEntities.forEach(entityManager::persist);
    }

    @Override
    public IndexedSeq<FooEntity> findAll() {
        return Vector.ofAll(entityManager.createQuery("FROM FooEntity f", FooEntity.class).getResultList());
    }
}
