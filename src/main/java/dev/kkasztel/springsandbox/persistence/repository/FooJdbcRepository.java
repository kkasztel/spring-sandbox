package dev.kkasztel.springsandbox.persistence.repository;

import dev.kkasztel.springsandbox.persistence.entity.FooEntity;
import io.vavr.collection.IndexedSeq;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import jakarta.annotation.Nonnull;

import io.vavr.collection.Vector;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import static io.vavr.collection.Vector.collector;

@Repository
@RequiredArgsConstructor
public class FooJdbcRepository implements FooRepository {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public void insert(IndexedSeq<FooEntity> fooEntities) {
        jdbcTemplate.batchUpdate(
                "INSERT INTO foo_entity VALUES(?,?)",
                new BatchPreparedStatementSetter() {

                    @Override
                    public void setValues(@Nonnull PreparedStatement ps, int i) throws SQLException {
                        ps.setObject(1, fooEntities.get(i).getId());
                        ps.setString(2, fooEntities.get(i).getData());
                    }

                    @Override
                    public int getBatchSize() {
                        return fooEntities.size();
                    }
                }
        );
    }

    @Override
    public IndexedSeq<FooEntity> findAll() {
        return jdbcTemplate.queryForStream(
                        "SELECT * FROM foo_entity",
                        new BeanPropertyRowMapper<>(FooEntity.class)
                )
                .collect(collector());
    }
}
