package dev.kkasztel.springsandbox.persistence.repository;

import dev.kkasztel.springsandbox.persistence.entity.FooEntity;
import dev.kkasztel.springsandbox.persistence.entity.FooEntity_;
import io.vavr.collection.IndexedSeq;
import io.vavr.collection.Seq;
import io.vavr.collection.Vector;

import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Subquery;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class FooSpecRepository {

    private final FooJpaRepository repository;

    public IndexedSeq<FooEntity> findWithResources(Seq<String> resources) {
        return Vector.ofAll(repository.findAll(spec(resources.toJavaSet())));
    }

    private Specification<FooEntity> spec(Set<String> resources) {
        return (root, query, cb) -> {
            Subquery<UUID> subquery = query.subquery(UUID.class);
            Root<FooEntity> subRoot = subquery.from(FooEntity.class);
            subquery.select(subRoot.get(FooEntity_.id))
                    .where(subRoot.join(FooEntity_.resources).in(resources))
                    .groupBy(subRoot.get(FooEntity_.id))
                    .having(cb.equal(cb.count(cb.literal(1)), resources.size()));
            return root.get(FooEntity_.id).in(subquery);
        };
    }
}
