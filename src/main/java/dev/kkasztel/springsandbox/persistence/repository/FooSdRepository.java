package dev.kkasztel.springsandbox.persistence.repository;

import dev.kkasztel.springsandbox.persistence.entity.FooEntity;
import io.vavr.API;
import io.vavr.collection.IndexedSeq;
import io.vavr.collection.Vector;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class FooSdRepository implements FooRepository {

    private final FooJpaRepository fooRepository;

    @Override
    public void insert(IndexedSeq<FooEntity> fooEntities) {
        fooRepository.saveAll(fooEntities);
    }

    @Override
    public IndexedSeq<FooEntity> findAll() {
        return Vector.ofAll(fooRepository.findAll());
    }
}
